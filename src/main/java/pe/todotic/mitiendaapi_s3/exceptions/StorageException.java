package pe.todotic.mitiendaapi_s3.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class StorageException extends RuntimeException{

    public StorageException(String message){
        super(message);
    }
}
