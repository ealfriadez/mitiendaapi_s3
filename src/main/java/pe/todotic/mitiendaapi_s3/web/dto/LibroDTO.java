package pe.todotic.mitiendaapi_s3.web.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class LibroDTO {

    @NotNull(message = "El titulo es obligatorio")
    @Size(min = 3, max = 100, message = "El título debe tener {min} caracteres como mínimo y {max} caracteres como máximo")
    private String titulo;

    @Pattern(regexp = "[a-z0-9-]+")
    @NotNull(message = "El slug es obligatorio")
    private String slug;

    @NotBlank(message = "La descripción es obligatoria")
    private String descripcion;

    @NotBlank(message = "La portada es obligatoria")
    private String rutaPortada;

    @NotBlank(message = "El archivo es obligatorio")
    private String rutaArchivo;

    @NotNull(message = "El precio es obligatorio")
    @PositiveOrZero(message = "El precio debe ser mayor o igual a 0")
    private Float precio;
}
