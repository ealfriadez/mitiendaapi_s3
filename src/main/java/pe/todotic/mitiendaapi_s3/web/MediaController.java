package pe.todotic.mitiendaapi_s3.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.todotic.mitiendaapi_s3.service.StorageService;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/media")
public class MediaController {

    @Autowired
    private StorageService storageService;

    Resource getResource(){
        return null;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @PostMapping("/upload")
    Map<String, String> upload(@RequestParam MultipartFile file){
        String ruta = storageService.store(file);

        Map<String, String> response = new HashMap<>();
        response.put("ruta", ruta);
        return response;
    }
}
